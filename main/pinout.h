#pragma once

#include "driver/adc.h"

// esp32
// i2c

// Analogue
#define WATER_TEMP GPIO_NUM_4
#define PRECHARGE GPIO_NUM_12
#define OIL_TEMP GPIO_NUM_13
#define PACK_VOLTAGE GPIO_NUM_15

#define DC_CURRENT GPIO_NUM_35
#define PP GPIO_NUM_36
#define CP GPIO_NUM_39

// Digital
#define LED GPIO_NUM_2
#define MAIN_CONTACTOR GPIO_NUM_5
#define CHARGE_CONTACTOR GPIO_NUM_14

#define CH_EN GPIO_NUM_17
#define VACUME_EN GPIO_NUM_18
#define PRESSURE_SWITCH GPIO_NUM_19
#define ESTOP_PRESSED GPIO_NUM_21

#define CAN0_R GPIO_NUM_22
#define CAN0_D GPIO_NUM_23

#define ENABLE_DEVICE GPIO_NUM_25
// #define SPARE_OUT GPIO_NUM_26

#define SCL_GPIO GPIO_NUM_22
#define SDA_GPIO GPIO_NUM_23

/// AC RELATED PINS

// Digital Out
#define AC_EN GPIO_NUM_16
#define SPARE_IN ENABLE_DEVICE  // GPIO_NUM_34 green wired

// Digital In
#define AC_DETECT GPIO_NUM_32
#define AC2_DETECT GPIO_NUM_26

// Analog In
#define AC_CURRENT GPIO_NUM_27
#define AC_V GPIO_NUM_33