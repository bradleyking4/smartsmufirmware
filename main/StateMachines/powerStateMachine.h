#include <stdint.h>

typedef enum CHARGER_STATES {
  CHARGE_IDLE,
  CHARGE_J17772,
  CHARGE_AC,
  CHARGE_PRE,
  CHARGE_RUN,
  CHARGE_ERROR,
  CHARGE_NUM_STATES,
} CHARGER_STATES;

void charger_setupTransitions();

void charger_init();
void charger_update();
