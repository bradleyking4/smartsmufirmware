///
typedef enum J1772_SPEED_STATES { J1172_IDLE, J1172_CONNECTED, J1172_READY, J1172_CHARGING, J1172_FINISHED, J1172_ERROR, J1772_SPEED_NUM_STATES } SPEED_STATES;

class J1772_SPEED {
 private:
  SPEED_STATES currentState, newState;

  bool checkForTransitions(SPEED_STATES from);
  bool (*validTransition[J1772_SPEED_NUM_STATES][J1772_SPEED_NUM_STATES])();
  void createTransition(bool (*validTransition[J1772_SPEED_NUM_STATES][J1772_SPEED_NUM_STATES])(), SPEED_STATES from, SPEED_STATES to, bool (*function)());

 public:
  void J1772_setupTransitions();
  void J1772_init();
  void J1772_update();
};
