#include "powerStateMachine.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "../VIRT_IO/virtual_IO.h"
#include "../pinout.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "i2cdev.h"
// #include "speedStateMachine.h"

#include "../Peripherals/can.h"

CHARGER_STATES currentState, newState;

bool started = true;
bool MainContactorEnabled = false;
bool ChargeContractorEnabled = false;

bool (*validTransition[CHARGE_NUM_STATES][CHARGE_NUM_STATES])();

inline void createTransition(bool (*validTransition[CHARGE_NUM_STATES][CHARGE_NUM_STATES])(), CHARGER_STATES from, CHARGER_STATES to, bool (*function)()) {
  validTransition[from][to] = function;
}

uint64_t changedTime_micro = 0;
bool estopPressed = 0;
float packVoltage = 0;
float prechargeVoltage = 0;

/*key in position 1*/
// not sure how to detect this.
bool transistion_idle() { return true; }

/*PP*/
bool transistion_idle_j1772() {
  float ppVoltage = IO_readADC(PP);
  if (1 < ppVoltage && ppVoltage < 2) {
    return true;
  }
  return false;
}

bool transistion_j1772_idle() { return !transistion_idle_j1772(); }

/*240v detected*/
bool transistion_idle_AC() {
  if (!IO_read(AC_DETECT)) {
    return true;
  }
  return false;
}
bool transistion_AC_idle() { return !transistion_idle_AC(); }

/*rey removed*/
bool transistion_idle_acc() {
  /// recieved can packet from ecu
  return false;
}

/*is everything shutdown?*/
bool transistion_acc_run() {
  if (abs(prechargeVoltage) < 10) {
    return true;
  }

  // precharge was successful
  return false;
}

// precharge took too long
bool transistion_acc_error() {
  if (changedTime_micro > 1000 * 1000 * 30) {
    return true;
  }
  return false;
}

bool transistion_run_idle() {
  // shutdown ordered
  return false;
}

void charger_setupTransitions() {
  memset(validTransition, 0, CHARGE_NUM_STATES * CHARGE_NUM_STATES);

  // create charger managing states
  createTransition(validTransition, CHARGE_IDLE, CHARGE_J17772, transistion_idle_j1772);
  createTransition(validTransition, CHARGE_IDLE, CHARGE_AC, transistion_idle_AC);

  createTransition(validTransition, CHARGE_J17772, CHARGE_IDLE, transistion_j1772_idle);
  createTransition(validTransition, CHARGE_AC, CHARGE_IDLE, transistion_AC_idle);

  // create car running states
  createTransition(validTransition, CHARGE_IDLE, CHARGE_PRE, transistion_idle_acc);
  createTransition(validTransition, CHARGE_PRE, CHARGE_RUN, transistion_acc_run);
  createTransition(validTransition, CHARGE_PRE, CHARGE_ERROR, transistion_acc_error);

  createTransition(validTransition, CHARGE_RUN, CHARGE_IDLE, transistion_run_idle);
}

bool checkForTransitions(CHARGER_STATES from) {
  for (int i = 0; i < CHARGE_NUM_STATES; ++i) {
    if (validTransition[from][i] && (*validTransition[from][i])()) {
      newState = (CHARGER_STATES)i;
      return true;
    }
  }
  return false;
}

inline void RelayOFF(int pin) { IO_write(pin, 0); }

inline void RelayON(int pin) { IO_write(pin, 1); }

void charger_init() {
  currentState = CHARGE_IDLE;
  newState = CHARGE_IDLE;
  charger_setupTransitions();
}

void charger_changeStateFrom(CHARGER_STATES from) {
  switch (from) {
    case CHARGE_IDLE:
      RelayON(MAIN_CONTACTOR);
      MainContactorEnabled = true;
      break;
    case CHARGE_J17772:
      break;
    case CHARGE_AC:

      break;
    case CHARGE_PRE:

      break;
    case CHARGE_RUN:

      break;
    case CHARGE_ERROR:

      break;

    default:
      break;
  }
}

void charger_handleState(CHARGER_STATES state) {
  switch (state) {
    case CHARGE_IDLE:
      break;
    case CHARGE_J17772:
      break;
    case CHARGE_AC:
      break;
    case CHARGE_PRE:

      break;
    case CHARGE_RUN:
      break;
    case CHARGE_ERROR:
      break;
    default:
      break;
  }
}

void charger_changeStateTo(CHARGER_STATES to) {
  switch (to) {
    case CHARGE_IDLE:
      RelayOFF(MAIN_CONTACTOR);
      RelayOFF(CHARGE_CONTACTOR);
      MainContactorEnabled = false;
      ChargeContractorEnabled = false;

      break;
    case CHARGE_J17772:
      RelayON(CHARGE_CONTACTOR);
      ChargeContractorEnabled = true;
      RelayON(SPARE_IN);
      vTaskDelay(50);
      RelayOFF(SPARE_IN);
      break;
    case CHARGE_AC:
      RelayON(AC_EN);
      vTaskDelay(50);
      RelayOFF(AC_EN);

      RelayON(CHARGE_CONTACTOR);
      ChargeContractorEnabled = true;

      break;
    case CHARGE_RUN:
      // send can message for OKAY

      break;
    case CHARGE_PRE:

      break;
    case CHARGE_ERROR:
      // turn off AC RELAY FIRST iif can.
      RelayOFF(MAIN_CONTACTOR);
      RelayOFF(CHARGE_CONTACTOR);
      MainContactorEnabled = false;
      ChargeContractorEnabled = false;
      break;

    default:
      break;
  }
}

void charger_changeState(CHARGER_STATES __newState) {
  charger_changeStateFrom(currentState);
  ESP_LOGI("Power", "Changing State from %d to %d", currentState, __newState);

  currentState = __newState;
  changedTime_micro = esp_timer_get_time();
  charger_changeStateTo(currentState);
}

void check_safety_loop() {
  estopPressed = !IO_read(ESTOP_PRESSED);

  if (estopPressed && currentState != CHARGE_ERROR) {
    esp_log_write(ESP_LOG_ERROR, "%s/n", "Estop was pressed");
    charger_changeState(CHARGE_ERROR);
  }
}

long currentTime_ms;
long lastHeartbeat;
const long heartbeatInterval = 1000;

int count = 0;

void charger_update() {
  if (checkForTransitions(currentState)) {
    charger_changeState(newState);
  }

  heartbeat.data[0] = currentState;

  heartbeat.data[1] = estopPressed;

  packVoltage = ((float)IO_readADC(PACK_VOLTAGE) / (2 ^ 12)) * 132;
  prechargeVoltage = packVoltage - (((float)IO_readADC(PRECHARGE) / (2 ^ 12)) * 132);

  check_safety_loop();

  currentTime_ms = esp_timer_get_time() / 1000;
  if (currentTime_ms > lastHeartbeat + heartbeatInterval) {
    lastHeartbeat = currentTime_ms;

    heartbeat.data[2] = (uint8_t)packVoltage;
    heartbeat.data[3] = (uint8_t)prechargeVoltage;
    heartbeat.data[4] = MainContactorEnabled & ChargeContractorEnabled << 1;

    heartbeat.data[7] = ++count;

    acStatus.data[0] = (!IO_read(AC_DETECT)) | (!IO_read(AC2_DETECT) << 1);
    // printf("AC %d  AC2 %d\n", !IO_read(AC_DETECT), !IO_read(AC2_DETECT));
    uint16_t acVoltage = ((float)IO_readADC(AC_V) / (2 ^ 12)) * 400;
    int16_t acCurrent = (((float)IO_readADC(AC_CURRENT) / (2 ^ 12)) - (0.5)) * 20;

    printf("packVoltage %d    %f\n", IO_readADC(PACK_VOLTAGE), packVoltage);

    acStatus.data[1] = acVoltage >> 8;
    acStatus.data[2] = acVoltage & 0xff;

    acStatus.data[3] = acCurrent >> 8;
    acStatus.data[4] = acCurrent & 0xff;

    can_transmit(&heartbeat, 10);
    vTaskDelay(10);
    can_transmit(&acStatus, 10);
  }

  charger_handleState(currentState);
}