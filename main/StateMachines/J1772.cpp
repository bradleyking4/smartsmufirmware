#include "J1772.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "../VIRT_IO/virtual_IO.h"
#include "../pinout.h"
#include "esp_log.h"

inline void J1772_SPEED::createTransition(bool (*validTransition[J1772_SPEED_NUM_STATES][J1772_SPEED_NUM_STATES])(), SPEED_STATES from, SPEED_STATES to, bool (*function)()) {
  validTransition[from][to] = function;
}

int accelPedal;

/*Vehicle connected*/
bool transistion_A() {
  float cpVoltage = IO_readADC(CP);
  const float expected = 9;
  const float bounds = 1;
  return (expected + bounds) > cpVoltage && (expected - bounds) < cpVoltage;
}

/*ready to accept energy*/
bool transistion_B() {
  float cpVoltage = IO_readADC(CP);
  const float expected = 4.5;
  const float bounds = 3;
  return (expected + bounds) > cpVoltage && (expected - bounds) < cpVoltage;
}

/*EVSE disconnected*/
bool transistion_C() {
  float cpVoltage = IO_readADC(CP);
  const float expected = 0;
  const float bounds = 1;
  return (expected + bounds) > cpVoltage && (expected - bounds) < cpVoltage;
}

void J1772_SPEED::J1772_setupTransitions() {
  memset(validTransition, 0, J1772_SPEED_NUM_STATES * J1772_SPEED_NUM_STATES);
  // from charger_OFF to charger_ACC
  createTransition(validTransition, J1172_IDLE, J1172_CONNECTED, transistion_A);
  createTransition(validTransition, J1172_CONNECTED, J1172_READY, transistion_B);
  createTransition(validTransition, J1172_READY, J1172_IDLE, transistion_C);
}

bool J1772_SPEED::checkForTransitions(SPEED_STATES from) {
  for (int i = 0; i < J1772_SPEED_NUM_STATES; ++i) {
    if (validTransition[from][i] && (*validTransition[from][i])()) {
      newState = (SPEED_STATES)i;
      return true;
    }
  }
  return false;
}

void J1772_SPEED::J1772_init() { J1772_setupTransitions(); }

void speed_changeStateFrom(SPEED_STATES from) {
  switch (from) {
    case J1172_IDLE:
      /* code */
      break;
    case J1172_CONNECTED:

      break;
    case J1172_READY:

      break;
    case J1172_ERROR:
      /* code */
      break;
    default:
      break;
  }
}
void speed_changeStateTo(SPEED_STATES to) {
  switch (to) {
    case J1172_IDLE:
      /* code */
      break;
    case J1172_CONNECTED:
      // enable contactors and relay.

      break;
    case J1172_READY:
      // engage relay CH_EN

      break;

    case J1172_CHARGING:
      // tell charger to start.
      /* code */
      break;

    case J1172_ERROR:
      /* code */
      break;

    case J1172_FINISHED:
      // open CH_EN

      /* code */
      break;

    default:
      break;
  }
}
void speed_handleState(SPEED_STATES state) {
  switch (state) {
    case J1172_IDLE:
      /* code */
      break;
    case J1172_CONNECTED:
      // read duty cycle
      break;
    case J1172_READY:
      // read duty cycle
      break;
    case J1172_CHARGING:
      // read duty cycle

      break;
    case J1172_ERROR:
      /* code */
      break;
    default:
      break;
  }
}

void J1772_SPEED::J1772_update() {
  if (checkForTransitions(currentState)) {
    speed_changeStateFrom(currentState);
    currentState = newState;
    speed_changeStateTo(currentState);
  }

  speed_handleState(currentState);
}
