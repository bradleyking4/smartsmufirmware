/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>

#include "StateMachines/powerStateMachine.h"
// #include "StateMachines/powerStateMachine.h"
#include "Peripherals/can.h"
#include "VIRT_IO/esp32_io.h"
#include "VIRT_IO/i2c_expander.h"
#include "VIRT_IO/virtual_IO.h"
#include "can_ota.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "pinout.h"

MCP23x17 exp_gear, exp_kelly;
esp32_IO esp32_io;

typedef struct esp_custom_app_desc {
  char desc[20];
  uint32_t baseCanAddress;
  uint32_t release;
  /* data */
} esp_custom_app_desc_t;

extern "C" {
const __attribute__((section(".rodata_custom_desc"))) esp_custom_app_desc_t custom_app_desc = {"Tim Is A Pleb", 0x420, 0xBEEF};

void app_main();
}

void init_pins() {
  IO_setGPIO(AC_EN, OUTPUT_LOW);
  IO_setGPIO(SPARE_IN, OUTPUT_LOW);

  IO_setGPIO(AC_DETECT, INPUT);
  IO_setGPIO(AC2_DETECT, INPUT);

  // IO_setGPIO(REV_SW, OUTPUT_LOW);
  // IO_setGPIO(FWD_SW, OUTPUT_LOW);

  // IO_setGPIO(CONTACTOR_MAIN, OUTPUT_LOW);
  // IO_setGPIO(KELLY_POWER, OUTPUT_LOW);
  // IO_setGPIO(CONTACTOR_MOTOR_DRIVE, OUTPUT_LOW);
  // IO_setGPIO(CONTACTOR_CHARGE, OUTPUT_LOW);
  // IO_setGPIO(CONTACTOR_DCDC, OUTPUT_LOW);

  // IO_setGPIO(CONTACTOR_SPARE_1, OUTPUT_LOW);
  // IO_setGPIO(CONTACTOR_SPARE_2, OUTPUT_LOW);

  // IO_setGPIO(GEARBOX_MOTOR_DIR_A, OUTPUT_LOW);
  // IO_setGPIO(GEARBOX_MOTOR_DIR_B, OUTPUT_LOW);

  // IO_setGPIO(GEARSTICK_IOEXP_INT, INPUT);

  // IO_setGPIO(ADC_GEAR_POS, INPUT_ADC);
  // IO_setGPIO(ADC_GEAR_MOTOR_CURRENT, INPUT_ADC);
  // IO_setGPIO(ADC_WATER_TEMPERATURE, INPUT_ADC);
  // IO_setGPIO(ADC_OIL_TEMP, INPUT_ADC);
  // IO_setGPIO(ADC_ACCEL_1, INPUT_ADC);
  // IO_setGPIO(ADC_ACCEL_2, INPUT_ADC);

  // IO_setGPIO(GEARSTICK_3, INPUT);
  // IO_setGPIO(GEARSTICK_4, INPUT);
  // IO_setGPIO(GEARSTICK_5, INPUT);

  // IO_setGPIO(GEARSTICK_8, INPUT);
  // IO_setGPIO(GEARSTICK_9, INPUT);
  // IO_setGPIO(GEARSTICK_10, INPUT);

  // IO_setGPIO(STOP_LIGHT, INPUT);
  // IO_setGPIO(STARTMOTOR, INPUT);

  // IO_setGPIO(LOCKOUT, INPUT);

  // IO_setGPIO(IO_1, OUTPUT);
  // IO_setGPIO(IO_2, INPUT);
  // IO_setGPIO(IO_3, INPUT);
  // IO_setGPIO(IO_4, INPUT);
}

static void brakeManger(void* arg) {
  while (1) {
    if (!IO_read(PRESSURE_SWITCH)) {
      IO_write(VACUME_EN, 1);
      vTaskDelay(100 * 20);  // 20 secconds.
      IO_write(VACUME_EN, 0);
    }
    vTaskDelay(10);  // 100ms polling.
  }
}

void app_main() {
  printf("Hello world! %s\n", custom_app_desc.desc);

  /* Print chip information */
  esp_chip_info_t chip_info;
  esp_chip_info(&chip_info);
  printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ", chip_info.cores, (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
         (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

  printf("silicon revision %d, ", chip_info.revision);

  printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024), (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

  esp32_io.init();
  IO_addHandler(&esp32_io);

  init_pins();

  can_app_main(custom_app_desc.baseCanAddress);

  charger_init();
  vTaskDelay(100);
  can_ota.approve();

  IO_write(AC_EN, true);
  printf("Toggle Relay\n");
  vTaskDelay(50);
  IO_write(AC_EN, false);

  vTaskDelay(100);

  IO_write(SPARE_IN, true);
  printf("Toggle RelayBack\n");
  vTaskDelay(50);
  IO_write(SPARE_IN, false);

  xTaskCreate((TaskFunction_t)(&brakeManger), "brakeManager", 1024 * 4, NULL, 12, NULL);
  can_message_t message;

  long currentTime_ms;
  long lastUpdate = 0;
  const long updateInterval = 500;
  bool otaNotInProgress = true;

  while (1) {
    currentTime_ms = esp_timer_get_time() / 1000;
    if (currentTime_ms > lastUpdate + updateInterval) {
      lastUpdate = currentTime_ms;
      if (otaNotInProgress) charger_update();
    }

    if (xQueueReceive(can_smu_send_queue, &message, 100) == pdTRUE) {
      switch (message.identifier) {
        case 1:
          if (message.data[4] == 1) {
            can_ota.begin(message.data);
            // otaNotInProgress = false;
            can_message_t responceMessage;
            responceMessage.identifier = 0x421;
            can_transmit(&responceMessage, 10);
          }
          if (message.data[5] == 1) {
            can_ota.end();
            // otaNotInProgress = true;
          }
          break;
        case 2:
          can_ota.update(message.data, message.data_length_code);
          can_message_t responceMessage;
          responceMessage.identifier = 0x421;
          can_transmit(&responceMessage, 10);

          break;

        default:
          break;
      }
    }
  }
}
