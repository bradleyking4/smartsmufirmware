#define QUAD_ENC_NAME "encoder"
#define QUAD_ENC_PRIORITY 3
#define QUAD_ENC_STACK 1024 * 3
#define TAG_QUAD_ENC "ENC"

#define BRAKE_NAME "Brake"
#define BRAKE_PRIORITY 4
#define BRAKE_STACK 1024 * 1
#define BRAKE_ADC "Brake"
