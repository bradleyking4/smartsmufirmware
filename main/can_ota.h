///
#pragma once
#include <stdint.h>

#include "esp_flash_partitions.h"
#include "esp_ota_ops.h"

class Can_ota {
 private:
  const esp_partition_t* update_partition;
  int lastMesageID = 0;
  esp_ota_handle_t update_handle = 0;

 public:
  Can_ota();
  esp_err_t begin(uint8_t data[8]);
  esp_err_t update(uint8_t data[8], uint8_t dataLength);
  esp_err_t end();
  esp_err_t approve();
};

extern Can_ota can_ota;