#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)
 COMPONENT_SRCDIRS :=  $(shell find $(COMPONENT_PATH)/./ -type d -printf './%P ')


# COMPONENT_SRCDIRS:= . ./Peripherals ./StateMachines ./VIRT_IO 
 $(warning COMPONENT_SRCDIRS=$(COMPONENT_SRCDIRS))
