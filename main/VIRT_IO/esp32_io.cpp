#include "esp32_io.h"

#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>

#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "io_base.h"

#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "../pinout.h"
#include "esp_log.h"
// A0, A1, A2 pins are grounded

#define SDA_GPIO SDA
#define SCL_GPIO SCL

#define DEFAULT_VREF 880  // Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES 1   // Multisampling
bool adc1Init = false;
bool adc2Init = false;

static const adc1_channel_t channel = ADC1_GPIO36_CHANNEL;
static esp_adc_cal_characteristics_t *adc_chars;

void fuel_initADC() {}

const uint8_t ADC1_PINS[] = {
    ADC1_CHANNEL_0_GPIO_NUM, ADC1_CHANNEL_1_GPIO_NUM, ADC1_CHANNEL_2_GPIO_NUM, ADC1_CHANNEL_3_GPIO_NUM,
    ADC1_CHANNEL_4_GPIO_NUM, ADC1_CHANNEL_5_GPIO_NUM, ADC1_CHANNEL_6_GPIO_NUM, ADC1_CHANNEL_7_GPIO_NUM,
};

const uint8_t ADC2_PINS[] = {
    ADC2_CHANNEL_0_GPIO_NUM, ADC2_CHANNEL_1_GPIO_NUM, ADC2_CHANNEL_2_GPIO_NUM, ADC2_CHANNEL_3_GPIO_NUM, ADC2_CHANNEL_4_GPIO_NUM,
    ADC2_CHANNEL_5_GPIO_NUM, ADC2_CHANNEL_6_GPIO_NUM, ADC2_CHANNEL_7_GPIO_NUM, ADC2_CHANNEL_8_GPIO_NUM, ADC2_CHANNEL_9_GPIO_NUM,
};

void intADC(int pin) {
  adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
  esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);

  for (int i = 0; i < sizeof(ADC1_PINS); i++) {
    if (pin == ADC1_PINS[i]) {
      if (!adc1Init) {
        adc1Init = true;
        adc1_config_width(ADC_WIDTH_BIT_12);
      }
      adc1_config_channel_atten((adc1_channel_t)i, ADC_ATTEN_DB_11);
    }
  }

  for (int i = 0; i < sizeof(ADC2_PINS); i++) {
    if (pin == ADC2_PINS[i]) {
      // if (!adc2Init) {
      //   adc2Init = true;
      //   adc2_config_width(ADC_WIDTH_BIT_12);
      // }

      adc2_config_channel_atten((adc2_channel_t)i, ADC_ATTEN_DB_11);
    }
  }
};

// we assume that all virtual pins are continuous
void esp32_IO::init() {
  ESP_LOGW("esp32_IO", "initing");
  this->firstPin = 0;
  gpio_install_isr_service(0);
}

int esp32_IO::readADC(int pin) {
  if (pins[pin] != INPUT_ADC) {
    ESP_LOGE("esp32_IO", "Pin %d is not configured as an ADC\nChanging to ADC pin", pin);
    pins[pin] = INPUT_ADC;
    intADC(pin);
  }

  uint32_t adc_reading = 0;
  for (int i = 0; i < sizeof(ADC1_PINS); i++) {
    if (pin == ADC1_PINS[i]) {
      ESP_LOGI("esp32_IO", "Reading from pin %d for adc1 channel %d", pin, i);
      for (int j = 0; j < NO_OF_SAMPLES; j++) {
        adc_reading += adc1_get_raw((adc1_channel_t)i);
      }
      adc_reading /= NO_OF_SAMPLES;
    }
  }

  for (int i = 0; i < sizeof(ADC2_PINS); i++) {
    if (pin == ADC2_PINS[i]) {
      ESP_LOGI("esp32_IO", "Reading from pin %d for adc2 channel %d", pin, i);
      for (int j = 0; j < NO_OF_SAMPLES; j++) {
        int raw = 0;
        adc2_get_raw((adc2_channel_t)i, ADC_WIDTH_BIT_12, &raw);
        adc_reading += raw;
      }
      adc_reading /= NO_OF_SAMPLES;
    }
  }

  return esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);  // mV
}
void esp32_IO::writeADC(int pin, int value) {}

void esp32_IO::write(int pin, bool state) {
  ESP_LOGI("esp32_IO", "Setting pin %d to %d", pin, state);
  gpio_set_level((gpio_num_t)pin, state);
}

bool esp32_IO::read(int pin) { return gpio_get_level((gpio_num_t)pin); }

void esp32_IO::setGPIO(int pin, pinMode mode) {
  gpio_pad_select_gpio((gpio_num_t)pin);
  pins[pin] = mode;
  switch (mode) {
    case INPUT_ADC:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_pull_mode((gpio_num_t)pin, GPIO_FLOATING);
      intADC(pin);
      break;
    case INPUT:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_pull_mode((gpio_num_t)pin, GPIO_FLOATING);
      break;
    case INPUT_LOW:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_pull_mode((gpio_num_t)pin, GPIO_PULLDOWN_ONLY);
      break;
    case INPUT_HIGH:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_pull_mode((gpio_num_t)pin, GPIO_PULLUP_ONLY);
      break;
    case OUTPUT_LOW:
    case OUTPUT:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_OUTPUT);
      gpio_set_level((gpio_num_t)pin, 0);
      break;

    case OUTPUT_HIGH:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_OUTPUT);
      gpio_set_level((gpio_num_t)pin, 1);
      break;
    default:
      break;
  }
};

void esp32_IO::addCallback(int pin, pinMode mode, void(*callback(void *arg))) {
  switch (mode) {
    case INTERRUPT_ANY:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_intr_type((gpio_num_t)pin, GPIO_INTR_ANYEDGE);
      // gpio_isr_handler_add((gpio_num_t)pin, (gpio_isr_t)callback, NULL);
      break;

    case INTERRUPT_RISING:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_intr_type((gpio_num_t)pin, GPIO_INTR_POSEDGE);
      // gpio_isr_handler_add((gpio_num_t)pin, (gpio_isr_t)callback, NULL);
      break;

    case INTERRUPT_FALLING:
      gpio_set_direction((gpio_num_t)pin, GPIO_MODE_INPUT);
      gpio_set_intr_type((gpio_num_t)pin, GPIO_INTR_NEGEDGE);
      // gpio_isr_handler_add((gpio_num_t)pin, (gpio_isr_t)callback, NULL);
      break;

    default:
      gpio_set_intr_type((gpio_num_t)pin, GPIO_INTR_DISABLE);
      break;
  }
};
