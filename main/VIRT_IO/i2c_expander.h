#ifndef I2C_EXPANDER_H
#define I2C_EXPANDER_H

#include <mcp23x17.h>
#include <stdint.h>
#include <stdio.h>

#include "io_base.h"

class MCP23x17 : public IO_BASE {
 private:
  mcp23x17_t dev;
  const char* tag = "IO_mcp";
#define NUMBER_OF_PINS 16

 public:
  MCP23x17() : IO_BASE(NUMBER_OF_PINS) {}  // you can specify which base class constructor to call

  void init(int firstPin, int address, gpio_num_t intA, gpio_num_t intB);
  void setGPIO(int pin, pinMode mode);
  void write(int pin, bool state);
  bool read(int pin);

  void writeADC(int pin, int value){};
  int readADC(int pin) { return 0; };

  void addCallback(int pin, pinMode mode, void(*callback(void* arg)));
};

#endif