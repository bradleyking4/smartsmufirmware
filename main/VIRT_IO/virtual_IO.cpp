#include "virtual_IO.h"
#define LOG_LOCAL_LEVEL ESP_LOG_WARN
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "esp_log.h"
#define IO_COUNT 5
static IO_BASE* ios[IO_COUNT];

void init() {
  for (int i = 0; i < IO_COUNT; i++) {
    ios[i] = NULL;
  }
}

IO_BASE* getExpander(int pin) {
  for (int i = 0; i < IO_COUNT; i++) {
    if (ios[i] == NULL) continue;
    if (pin >= ios[i]->firstPin && pin <= (ios[i]->firstPin + ios[i]->numberOfPins)) {
      ESP_LOGI("TAG", "returning IO %d for pin %d", i, pin);
      return ios[i];
    }
  }
  ESP_LOGE("TAG", "NO IO found for pin %d", pin);
  return NULL;
}

void IO_addHandler(IO_BASE* io) {
  for (int i = 0; i < IO_COUNT; i++) {
    if (ios[i] == NULL) {
      ios[i] = io;
      return;
    };
  }
}

int offsetPin(IO_BASE* io, int pin) {
  pin -= io->firstPin;
  assert(pin >= 0 && pin <= io->numberOfPins);
  return pin;
}

void IO_setGPIO(int pin, pinMode mode) {
  IO_BASE* expander = getExpander(pin);
  if (expander == NULL) return;
  pin = offsetPin(expander, pin);
  expander->setGPIO(pin, mode);
}

void IO_write(int pin, bool state) {
  IO_BASE* expander = getExpander(pin);
  if (expander == NULL) return;
  int offset = offsetPin(expander, pin);
  expander->write(offset, state);
}

bool IO_read(int pin) {
  IO_BASE* expander = getExpander(pin);
  if (expander == NULL) return 0;
  pin = offsetPin(expander, pin);
  return expander->read(pin);
}

void IO_addCallback(int pin, pinMode mode, void(*callback(void* arg))) {
  IO_BASE* expander = getExpander(pin);
  if (expander == NULL) return;
  pin = offsetPin(expander, pin);
  expander->addCallback(pin, mode, callback);
}

int IO_readADC(int pin) {
  IO_BASE* expander = getExpander(pin);
  if (expander == NULL) return -1;
  pin = offsetPin(expander, pin);
  return expander->readADC(pin);
}

void IO_writeADC(int pin, int value) {
  IO_BASE* expander = getExpander(pin);
  if (expander == NULL) return;
  pin = offsetPin(expander, pin);
}
