#pragma once

#include <stdint.h>
#include <stdio.h>

#include "io_base.h"

class esp32_IO : public IO_BASE {
 private:
  const char* tag = "IO_ESP";
#define ESP32_NUMBER_OF_PINS 40

 public:
  esp32_IO() : IO_BASE(ESP32_NUMBER_OF_PINS) {}  // you can specify which base class constructor to call

  void init();
  void setGPIO(int pin, pinMode mode);
  void write(int pin, bool state);
  bool read(int pin);

  int readADC(int pin);
  void writeADC(int pin, int value);

  void addCallback(int pin, pinMode mode, void(*callback(void* arg)));
};
