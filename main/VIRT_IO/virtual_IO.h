
#pragma once
#include "i2c_expander.h"
#include "io_base.h"

#define OFF 0
#define ON 1

void init();

void IO_addHandler(IO_BASE* io);
void IO_setGPIO(int pin, pinMode mode);
void IO_write(int pin, bool state);
void IO_writeADC(int pin, int value);

bool IO_read(int pin);
int IO_readADC(int pin);

void IO_addCallback(int pin, pinMode mode, void (*callback(void* arg))());