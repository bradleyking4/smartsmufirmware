#include "can_ota.h"

#include <string.h>

#include "esp_log.h"

#define TAG "can_ota"

#define HASH_LEN 32 /* SHA-256 digest length */
Can_ota can_ota;

Can_ota::Can_ota() {}

uint32_t* buffer[2000];
int bufferPos = 0;

esp_err_t Can_ota::begin(uint8_t data[8]) {
  // check with codeID that we are being requested to update to the write file... somehow

  update_partition = esp_ota_get_next_update_partition(NULL);
  esp_err_t err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
    return err;
  }
  bufferPos = 0;
  lastMesageID = 0;
  ESP_LOGE(TAG, "esp_ota_begin");

  return true;
}

esp_err_t Can_ota::update(uint8_t data[8], uint8_t dataLength) {
  if (bufferPos < 1900) {
    memcpy(&buffer[bufferPos], data, dataLength);
    bufferPos += dataLength;
    return ESP_OK;

  } else {
    esp_err_t err;
    if (update_handle) {
      ESP_LOGW(TAG, "Writing %d bytes to flash!", bufferPos);

      err = esp_ota_write(update_handle, buffer, bufferPos);
      bufferPos = 0;
      if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error: esp_ota_write failed (%s)!", esp_err_to_name(err));
        return err;
      }
      return ESP_OK;
    }
    return ESP_FAIL;
  }
}

esp_err_t Can_ota::end() {
  esp_err_t err;
  err = esp_ota_end(update_handle);
  if (err != ESP_OK) {
    return err;
  }
  err = esp_ota_set_boot_partition(update_partition);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
    return err;
  }
  return ESP_OK;
}

static void print_sha256(const uint8_t* image_hash, const char* label) {
  char hash_print[HASH_LEN * 2 + 1];
  hash_print[HASH_LEN * 2] = 0;
  for (int i = 0; i < HASH_LEN; ++i) {
    sprintf(&hash_print[i * 2], "%02x", image_hash[i]);
  }
  ESP_LOGI(TAG, "%s: %s", label, hash_print);
}

esp_err_t Can_ota::approve() {
  uint8_t sha_256[HASH_LEN] = {0};
  esp_partition_t partition;

  // get sha256 digest for the partition table
  partition.address = ESP_PARTITION_TABLE_OFFSET;
  partition.size = ESP_PARTITION_TABLE_MAX_LEN;
  partition.type = ESP_PARTITION_TYPE_DATA;
  esp_partition_get_sha256(&partition, sha_256);
  print_sha256(sha_256, "SHA-256 for the partition table: ");

  // get sha256 digest for bootloader
  partition.address = ESP_BOOTLOADER_OFFSET;
  partition.size = ESP_PARTITION_TABLE_OFFSET;
  partition.type = ESP_PARTITION_TYPE_APP;
  esp_partition_get_sha256(&partition, sha_256);
  print_sha256(sha_256, "SHA-256 for bootloader: ");

  // get sha256 digest for running partition
  esp_partition_get_sha256(esp_ota_get_running_partition(), sha_256);
  print_sha256(sha_256, "SHA-256 for current firmware: ");

  const esp_partition_t* running = esp_ota_get_running_partition();
  esp_ota_img_states_t ota_state;
  if (esp_ota_get_state_partition(running, &ota_state) == ESP_OK) {
    if (ota_state == ESP_OTA_IMG_PENDING_VERIFY) {
      // run diagnostic function ...
      bool diagnostic_is_ok = true;
      if (diagnostic_is_ok) {
        ESP_LOGI(TAG, "Diagnostics completed successfully! Continuing execution ...");
        esp_ota_mark_app_valid_cancel_rollback();
      } else {
        ESP_LOGE(TAG, "Diagnostics failed! Start rollback to the previous version ...");
        esp_ota_mark_app_invalid_rollback_and_reboot();
      }
    }
  }
  return ESP_OK;
}