#pragma once

#include "driver/can.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#ifdef __cplusplus
extern "C" {
#endif

void can_app_main(uint32_t baseCanAddress);

extern QueueHandle_t can_smu_send_queue;

extern can_message_t heartbeat;
extern can_message_t acStatus;

#ifdef __cplusplus
}
#endif

/*

#pragma once

#include "driver/can.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#ifdef __cplusplus
extern "C" {
#endif

TaskHandle_t can_app_main();

extern can_message_t dashInfo;
extern can_message_t speedo;
extern can_message_t engineSpeed;

extern can_message_t sam;

extern can_message_t smuStatus;
extern can_message_t hb;



bool getCanStatus();

#ifdef __cplusplus
}

*/