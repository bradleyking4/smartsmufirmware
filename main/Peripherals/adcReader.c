// #include "adcReader.h"

// #include <stdint.h>

// #include "driver/adc.h"
// #include "esp_adc_cal.h"
// #include "freertos/FreeRTOS.h"
// #include "freertos/task.h"
// #include "pinout.h"
// #include "taskDefs.h"

// #define DEFAULT_VREF 1100  // Use adc2_vref_to_gpio() to obtain a better estimate
// #define NO_OF_SAMPLES 32   // Multisampling

// #define ADC_MAX_SAMPLE_COUNT 6
// #define ADC_POLL_RATE 10  // hz

// typedef struct ADC_SAMPLE {
//   uint8_t channel;
//   bool adc1;
//   uint16_t* value;
// } ADC_SAMPLE;

// uint8_t sampleCount = 0;
// ADC_SAMPLE samplestoRead[ADC_MAX_SAMPLE_COUNT];

// void adc_initADC() {
//   // adc1_config_width(ADC_WIDTH_BIT_12);
//   // adc2_config_width(ADC_WIDTH_BIT_12);
// }

// int adc_readVoltage(adc1_channel_t channel) {
//   adc1_config_channel_atten(channel, ADC_ATTEN_DB_11);

//   uint32_t adc_reading = 0;
//   for (int i = 0; i < NO_OF_SAMPLES; i++) {
//     adc_reading += adc1_get_raw(channel);
//   }
//   adc_reading /= NO_OF_SAMPLES;
//   return ((3.96f * adc_reading) / (1 << 12)) * 2 * 880;  // mV
// }

// int adc2_readVoltage(adc2_channel_t channel) {
//   adc2_config_channel_atten(channel, ADC_ATTEN_DB_11);

//   uint32_t adc_reading = 0;
//   for (int i = 0; i < NO_OF_SAMPLES; i++) {
//     if (
//         // adc2_get_raw(channel, ADC_WIDTH_BIT_12, &adc_reading) != ESP_OK)
//         1) {
//       // ESP_LOGE(TAG_ADC, "ADC2 Failed to read value");
//       return false;
//     }
//   }
//   adc_reading /= NO_OF_SAMPLES;
//   return ((3.96f * adc_reading) / (1 << 12)) * 2 * 880;  // mV
// }

// void adc_init() {
//   // xTaskCreate((TaskFunction_t)(&adc_thread), ADC_NAME, ADC_STACK, NULL, ADC_PRIORITY, NULL);
// }

// void adc_thread() {
//   adc_initADC();

//   TickType_t lastLoop = xTaskGetTickCount();

//   while (1) {
//     for (int i = 0; i < sampleCount; ++i) {
//       if (samplestoRead[i].adc1) {
//         (*samplestoRead[i].value) = adc_readVoltage(samplestoRead[i].channel);
//       } else {
//         (*samplestoRead[i].value) = adc2_readVoltage(samplestoRead[i].channel);
//       }
//     }

//     vTaskDelayUntil(&lastLoop, (1000 / ADC_POLL_RATE / portTICK_PERIOD_MS));
//   }
// }

// void adc_addSample(bool adc1, uint8_t channel, uint16_t* result) {
//   if (sampleCount == ADC_MAX_SAMPLE_COUNT) {
//     // ESP_LOGE("ADC", "To many adc channels requested");
//     return;
//   }
//   samplestoRead[0].adc1 = adc1;
//   samplestoRead[0].channel = channel;
//   samplestoRead[0].value = result;
//   sampleCount++;
// }