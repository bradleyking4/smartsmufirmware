/* CAN Network Slave Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
 * The following example demonstrates a slave node in a CAN network. The slave
 * node is responsible for sending data messages to the master. The example will
 * execute multiple iterations, with each iteration the slave node will do the
 * following:
 * 1) Start the CAN driver
 * 2) Listen for ping messages from master, and send ping response
 * 3) Listen for start command from master
 * 4) Send data messages to master and listen for stop command
 * 5) Send stop response to master
 * 6) Stop the CAN driver
 */

#include "driver/can.h"

#include <stdio.h>
#include <stdlib.h>

#include "can.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

/* --------------------- Definitions and static variables ------------------ */
// Example Configuration
#define DATA_PERIOD_MS 50
#define NO_OF_ITERS 3
#define ITER_DELAY_MS 1000
#define RX_TASK_PRIO 8    // Receiving task priority
#define TX_TASK_PRIO 9    // Sending task priority
#define CTRL_TSK_PRIO 10  // Control task priority
#define TX_GPIO_NUM 23
#define RX_GPIO_NUM 22
#define EXAMPLE_TAG "CAN Slave"

static const can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(TX_GPIO_NUM, RX_GPIO_NUM, CAN_MODE_NORMAL);
static const can_timing_config_t t_config = CAN_TIMING_CONFIG_500KBITS();
static const can_filter_config_t f_config = CAN_FILTER_CONFIG_ACCEPT_ALL();

can_message_t heartbeat = {.identifier = 0, .data_length_code = 8, .data = {0, 0, 0, 0, 0, 0, 0, 0}};
can_message_t acStatus = {.identifier = 3, .data_length_code = 8, .data = {0, 0, 0, 0, 0, 0, 0, 0}};

can_message_t chargeConfig = {.identifier = 0x1806E5F4, .data_length_code = 8, .data = {0, 0, 0, 0, 0, 0, 0, 0}};
can_message_t chargingStatus = {.identifier = 0x18FF50E5, .data_length_code = 8, .data = {0, 0, 0, 0, 0, 0, 0, 0}};

void setChargingConfig(uint16_t voltage, uint16_t current, bool disable) {
  chargeConfig.data[0] = voltage >> 8;
  chargeConfig.data[1] = voltage & 0xff;

  chargeConfig.data[2] = current >> 8;
  chargeConfig.data[3] = current & 0xff;

  chargeConfig.data[4] = disable;
}

void processChargingStatus(uint16_t* voltage, uint16_t* current) {
  *voltage = chargingStatus.data[0] << 8 & chargingStatus.data[1];
  *current = chargingStatus.data[2] << 8 & chargingStatus.data[3];
}

QueueHandle_t can_smu_send_queue;

/* --------------------------- Tasks and Functions -------------------------- */

static void can_transmit_task(void* arg) {
  can_message_t message;
  while (1) {
    if (can_receive(&message, pdMS_TO_TICKS(1000)) == ESP_OK) {
      // printf("Message received\n");
    } else {
      // printf("Failed to receive message\n");
      continue;
    }

    // Process received message
    if (message.flags & CAN_MSG_FLAG_EXTD) {
      printf("Message is in Extended Format\n");
    } else {
      // printf("Message is in Standard Format\n");
    }
    printf("ID is %d\n", message.identifier);
    // if (!(message.flags & CAN_MSG_FLAG_RTR)) {
    //   for (int i = 0; i < message.data_length_code; i++) {
    //     printf("Data byte %d = %d\n", i, message.data[i]);
    //   }
    // }

    if (heartbeat.identifier < message.identifier && message.identifier < heartbeat.identifier + 20) {
      message.identifier -= heartbeat.identifier;
      xQueueSend(can_smu_send_queue, &message, 10);
    }
  }
  vTaskDelete(NULL);
}

void can_app_main(uint32_t baseCanAddress) {
  heartbeat.identifier = baseCanAddress;
  acStatus.identifier += baseCanAddress;
  // Add short delay to allow master it to initialize first

  // xTaskCreatePinnedToCore(can_control_task, "CAN_ctrl", 4096, NULL, CTRL_TSK_PRIO, NULL, tskNO_AFFINITY);

  // Install CAN driver, trigger tasks to start
  // Install CAN driver
  if (can_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
    printf("Driver installed\n");
  } else {
    printf("Failed to install driver\n");
    return;
  }

  if (can_start() == ESP_OK) {
    printf("Driver started\n");
  } else {
    printf("Failed to start driver\n");
    return;
  }

  can_smu_send_queue = xQueueCreate(10, sizeof(can_message_t));

  xTaskCreatePinnedToCore(can_transmit_task, "CAN_tx", 4096, NULL, TX_TASK_PRIO, NULL, 1);

  // // Uninstall CAN driver
  // ESP_ERROR_CHECK(can_driver_uninstall());
  // ESP_LOGI(EXAMPLE_TAG, "Driver uninstalled");

  // // Cleanup
  // vSemaphoreDelete(ctrl_task_sem);
  // vSemaphoreDelete(stop_data_sem);
  // vSemaphoreDelete(done_sem);
  // vQueueDelete(tx_task_queue);
  // vQueueDelete(rx_task_queue);
}
